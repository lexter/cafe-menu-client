package com.duxcast.bohorest.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.duxcast.bohorest.services.SocketService;

import org.json.JSONObject;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            try {
                Intent intentForSend = new Intent(SocketService.broadcastAction);
                JSONObject jsonObject = new JSONObject();
                if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
                    context.startService(new Intent(context, SocketService.class));
                    jsonObject.put("action", "bootComplete");
                    intentForSend.putExtra("message", jsonObject.toString());
                    context.sendBroadcast(intentForSend);
                    context.startService(new Intent(context, SocketService.class));
                }
                if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SHUTDOWN)) {
                    jsonObject.put("action", "shutdown");
                    intentForSend.putExtra("message", jsonObject.toString());
                    context.sendBroadcast(intentForSend);
                }
                if (intent.getAction().equalsIgnoreCase(Intent.ACTION_REBOOT)) {
                    jsonObject.put("action", "reboot");
                    intentForSend.putExtra("message", jsonObject.toString());
                    context.sendBroadcast(intentForSend);
                }
                Log.i(getClass().getName(),jsonObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}