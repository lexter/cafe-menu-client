package com.duxcast.bohorest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.model.Item;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import java.util.List;

public class ListItemAdapter extends ArrayAdapter<Item> {


    public ListItemAdapter(Context context, List<Item> objects) {
        super(context, R.layout.listitem_view, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_view, parent, false);

            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.textViewName);
            holder.textViewPrice = (TextView) convertView.findViewById(R.id.textViewPrice);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(String.format("%s. %s",(position + 1),getItem(position).getName(getContext())));
        holder.textViewPrice.setText(String.format("%s x %s %s",SharedPrefUtils.countItem(getItem(position).getId(),getContext()),getItem(position).getData().optInt("price"),getContext().getString(R.string.valute)));

        return convertView;
    }



    private static class ViewHolder {
        public TextView name;
        public TextView textViewPrice;
    }

}
