package com.duxcast.bohorest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.duxcast.bohorest.R;
import com.duxcast.bohorest.model.Item;
import com.duxcast.bohorest.utils.LangUtils;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

public class GridAdapter extends ArrayAdapter<Item> {


    public GridAdapter(Context context, List<Item> objects) {
        super(context, R.layout.listitem_view, objects);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.menuitem_view, parent, false);
        TextView textViewName = (TextView) convertView.findViewById(R.id.textViewName);
        try {
            textViewName.setText(getItem(position).getName(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
            textViewName.setText("-");
        }


        int price = getItem(position).getData().optInt("price", -1);
        TextView textViewPrice = (TextView) convertView.findViewById(R.id.textViewPrice);
        if (getItem(position).getData().optBoolean("last_child", false)) {
            textViewPrice.setVisibility(View.VISIBLE);
            textViewPrice.setText(String.format("%s %s",price,getContext().getString(R.string.valute)));
        } else {
            textViewPrice.setVisibility(View.INVISIBLE);
        }

        ImageView image = (ImageView) convertView.findViewById(R.id.icon);
        Glide
            .with(getContext())
            .load(getItem(position).getPictureFile(getContext()))
            .crossFade()
            .into(image);

        return convertView;
    }

}
