package com.duxcast.bohorest.db;

/**
 * Created by admin on 14.03.16.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static final String 	TAG 				= "DbOpenHelper";
    private static final String		DATABASE_NAME		= "menu.db";
    private static final int 		DATABASE_VERSION 	= 1;
    private static DbHelper mInstance;
    private static SQLiteDatabase db;

    private static final String[]	CREATE_SCRIPT 		= new String[] {

            "CREATE TABLE IF NOT EXISTS	 `menu`  	(`_id` 	INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`hash`                 char(32) not null,"+
                    "`name`	 				text not null,"+
                    "`name_lowcase`			text,"+
                    "`preview_text`	 		text,"+
                    "`preview_picture` 		text,"+
                    "`detail_text`	 		text,"+
                    "`detail_picture` 		text,"+
                    "`data`            		text,"+
                    "`last_child`        	int,"+
                    "`parent_id` 			int)",

    };


    public DbHelper(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DbHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbHelper(context);
        }
        return mInstance;
    }

    public synchronized SQLiteDatabase getDb() {
        if ((db == null) || (!db.isOpen())) {
            db = this.getWritableDatabase();
        }
        return db;
    }

    @Override
    public void close() {
        super.close();
        if (db != null) {
            db.close();
            db = null;
        }

        mInstance = null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String sql_line : CREATE_SCRIPT) {
            try {
                db.execSQL(sql_line);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static String DB_FILEPATH = "/data/data/com.duxgames.bohorest/databases/"+DATABASE_NAME;


}