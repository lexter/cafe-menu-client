package com.duxcast.bohorest.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class Frame {

    private String data;

    public Frame(String data) {
        this.data = data;
    }

    public Frame(InputStream inputStream) throws IOException {
        int first = inputStream.read();
        if (first==-1) {
            throw new IOException();
        }
        int length = (first<<16) | (inputStream.read()<<8) | (inputStream.read());
        if (length<0) length=0;
        byte [] utf8 = new byte[length];
        for (int i=0;i<length;i++) {
            utf8[i] = (byte)inputStream.read();
        }
        data = new String(utf8,Charset.forName("UTF8"));
    }

    public void send(OutputStream outputStream) throws IOException {
        byte [] utf8 = data.getBytes("UTF-8");
        outputStream.write((utf8.length & 0xFF0000)>>16);
        outputStream.write((utf8.length & 0x00FF00)>>8);
        outputStream.write((utf8.length & 0x0000FF));

        for (int i=0;i<utf8.length;i++) {
            outputStream.write(utf8[i]);
        }

    }

    public String getData() {
        return data;
    }
}
