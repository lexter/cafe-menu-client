package com.duxcast.bohorest.services;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import com.duxcast.bohorest.activities.HomeScreenActivity;
import com.duxcast.bohorest.dialogs.ConnectionDialog;
import com.duxcast.bohorest.dialogs.UpdateDialog;
import com.duxcast.bohorest.utils.Notifications;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class SocketService extends IntentService  {

    public static final String broadcastAction = "com.duxcast.bohorest.services.SocketServer.Send";

    Socket socket;
    BufferedInputStream in;
    BufferedOutputStream out;

    public SocketService() {
        super("SocketService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(broadcastAction);
        registerReceiver(receiver, mIntentFilter);

        Toast.makeText(getApplicationContext(), "SERVICE CREATE", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startForeground(1203, Notifications.generateNotification(getApplicationContext()));

        while(true) {
            try {
                ConnectionDialog.show(getApplicationContext());

                socket = new Socket(InetAddress.getByName(SharedPrefUtils.getServerIp(getApplicationContext())), SharedPrefUtils.getServerPort(getApplicationContext()));

                in = new BufferedInputStream(socket.getInputStream());
                out = new BufferedOutputStream(socket.getOutputStream());

                Intent intentForSend = new Intent(SocketService.broadcastAction);
                JSONObject object = new JSONObject();
                object.put("action", "checkLastBaseUpdate");
                intentForSend.putExtra("message", object.toString());
                sendBroadcast(intentForSend);

                intentForSend = new Intent(SocketService.broadcastAction);
                object = new JSONObject();
                object.put("action", "setTable");
                object.put("table", SharedPrefUtils.getTableNum(getApplicationContext()));
                intentForSend.putExtra("message", object.toString());
                sendBroadcast(intentForSend);

                ConnectionDialog.hide(getApplicationContext());

                while (socket.isConnected()) {
                    try {
                        Frame frame;

                        try {
                            frame = new Frame(in);
                        } catch (IOException e) {
                            break;
                        }

                        JSONObject js = new JSONObject(frame.getData());

                        Log.wtf("<- ",js.optString("action") + " *** " + js.toString());

                        if (js.optString("action").equals("changeOrder")) {
                            SharedPrefUtils.replaceData(js.optJSONArray("data"), getApplicationContext());
                            HomeScreenActivity.updateViews(getApplicationContext());
                        }
                        if (js.optString("action").equals("orderSeccess")) {
                            Intent intentact = new Intent(HomeScreenActivity.SUCCESS_ACTION);
                            sendBroadcast(intentact);
                        }
                        if (js.optString("action").equals("orderProcessed")) {
                            Intent intentact = new Intent(HomeScreenActivity.PROCESSED_ACTION);
                            sendBroadcast(intentact);
                        }
                        if (js.optString("action").equals("lastBaseUpdate")) {
                            if (SharedPrefUtils.getLastUpdate(getApplicationContext())<js.optLong("date",1)) {
                                intentForSend = new Intent(SocketService.broadcastAction);
                                object = new JSONObject();
                                object.put("action", "updateBase");
                                intentForSend.putExtra("message", object.toString());
                                sendBroadcast(intentForSend);
                            }
                        }
                        if (js.optString("action").equals("updateBase")) {
                            Intent intentact = new Intent(UpdateDialog.action);
                            intentact.putExtra("base",js.toString());
                            sendBroadcast(intentact);
                        }

                    } catch(Exception e) {
                        e.printStackTrace();
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(), "END LOOP", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        try {
            if (in != null) in.close();
            if (out != null) out.close();
            if (socket != null) socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        unregisterReceiver(receiver);
        Toast.makeText(getApplicationContext(), "SERVICE DESTROY", Toast.LENGTH_SHORT).show();
        super.onDestroy();

    }

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            synchronized (receiver) {
                try {
                    String extraMessage = intent.getStringExtra("message");
                    if (extraMessage == null) return;
                    Frame frame = new Frame(extraMessage);
                    frame.send(out);
                    out.flush();
                    JSONObject js = new JSONObject(extraMessage);
                    Log.wtf("-> ", js.optString("action") + " *** " + js.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Intent intentForSend = new Intent(HomeScreenActivity.ERROR_ACTION);
                        sendBroadcast(intentForSend);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

    };

}
