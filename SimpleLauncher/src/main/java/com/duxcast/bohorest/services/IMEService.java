package com.duxcast.bohorest.services;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.utils.LangUtils;

/**
 * Created by admin on 21.04.16.
 */
public class IMEService extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    private KeyboardView kv;
    private Keyboard keyboardEng,keyboardRus,selectedKeyboard;

    private boolean caps = false;
    private boolean rus = false;

    public static final int KEYCODE_EXIT = -7;
    public static final int KEYCODE_CHANGELANG = -8;

    @Override
    public View onCreateInputView() {
        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard_view, null);
        keyboardEng = new Keyboard(this, R.xml.qwerty_eng);
        keyboardRus = new Keyboard(this, R.xml.qwerty_rus);
        if (LangUtils.getLocale(getApplicationContext()).equals("en")) selectedKeyboard = keyboardEng;
        else selectedKeyboard = keyboardRus;
        kv.setKeyboard(selectedKeyboard);
        kv.setOnKeyboardActionListener(this);
        return kv;
    }

    private void playClick(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        InputConnection ic = getCurrentInputConnection();
        playClick(primaryCode);
        switch(primaryCode){
            case Keyboard.KEYCODE_DELETE :
                ic.deleteSurroundingText(1, 0);
                break;
            case Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                keyboardEng.setShifted(caps);
                kv.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            case KEYCODE_EXIT:
                requestHideSelf(0);
                kv.closing();
                System.out.println("EXIT");
                break;
            case KEYCODE_CHANGELANG:
                rus = !rus;
                if (rus) selectedKeyboard = keyboardRus;
                else selectedKeyboard = keyboardEng;
                kv.setKeyboard(selectedKeyboard);
                kv.invalidateAllKeys();
                kv.refreshDrawableState();
                break;
            default:
                char code = (char)primaryCode;
                if(Character.isLetter(code) && caps){
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code), 1);
        }
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {

    }

    @Override
    public boolean onEvaluateFullscreenMode() {
        return false;
    }

}