package com.duxcast.bohorest.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.duxcast.bohorest.utils.LangUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;


public class Item {

    int id;
    String hash;
    JSONObject name = new JSONObject();
    String preview_picture = null;
    JSONObject detail_text = new JSONObject();
    JSONObject data = new JSONObject();
    int parent_id = -1;
    ArrayList<Item> subItems = new ArrayList<>();

    private static String TABLE_NAME = "menu";

    public Item(JSONObject jsonObject,Context context) {
        id = jsonObject.optInt("id");

        hash =  jsonObject.optString("hash");

        name = jsonObject.optJSONObject("name");

        detail_text = jsonObject.optJSONObject("detail_text");

        preview_picture = jsonObject.optString("preview_picture");
        if (TextUtils.isEmpty(preview_picture)) preview_picture= jsonObject.optString("detail_picture");
        if (!preview_picture.contains("http://bohorest.ru")) preview_picture="http://bohorest.ru"+preview_picture;


        data = new JSONObject();
        if (jsonObject.optInt("price",-1)!=-1) {
            try {
                data.put("price",jsonObject.optInt("price"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            data.put("last_child",jsonObject.optBoolean("last_child", false));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("prop",jsonObject.optJSONArray("prop"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("sent",jsonObject.optBoolean("sent", false));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            data.put("min_count",jsonObject.optInt("min_count", 0));
        } catch (Exception e) {
            e.printStackTrace();
        }

        parent_id = jsonObject.optInt("parent_id");
        JSONArray jsonArray = jsonObject.optJSONArray("data");
        if (jsonArray==null) return;
        for (int i=0;i<jsonArray.length();i++) {
            subItems.add(new Item(jsonArray.optJSONObject(i),context));
        }

    }

    public Item(Cursor cursor) {
        try {
            id = cursor.getInt(cursor.getColumnIndex("_id"));
            hash = cursor.getString(cursor.getColumnIndex("hash"));
            try {name = new JSONObject(cursor.getString(cursor.getColumnIndex("name")));} catch (Exception e) {e.printStackTrace();}
            preview_picture = cursor.getString(cursor.getColumnIndex("preview_picture"));
            try {detail_text = new JSONObject(cursor.getString(cursor.getColumnIndex("detail_text")));} catch (Exception e) {e.printStackTrace();}
            data = new JSONObject(cursor.getString(cursor.getColumnIndex("data")));
            parent_id = cursor.getInt(cursor.getColumnIndex("parent_id"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public int save(SQLiteDatabase db) {
        try {
            ContentValues insertValues = new ContentValues();
            if (id!=0) insertValues.put("_id", id);
            insertValues.put("hash", hash);
            insertValues.put("name", name.toString());
            insertValues.put("preview_picture", preview_picture);
            if (detail_text!=null) insertValues.put("detail_text", detail_text.toString());
            insertValues.put("data", data.toString());
            insertValues.put("parent_id", parent_id);
            if (data.optBoolean("last_child",false)) {
                insertValues.put("last_child", 1);
            } else {
                insertValues.put("last_child", 0);
            }

            StringBuilder stringBuilder = new StringBuilder();
            if (name!=null) {
                stringBuilder.append(name.optString("rus", ""));
                stringBuilder.append(name.optString("eng", ""));
            }
            if (detail_text!=null) {
                stringBuilder.append(detail_text.optString("rus", ""));
                stringBuilder.append(detail_text.optString("eng", ""));
            }
            insertValues.put("name_lowcase", stringBuilder.toString().toLowerCase());


            id = (int) db.insert(TABLE_NAME, null, insertValues);

            for (Item item:subItems) {
                item.parent_id = id;
                item.save(db);
            }

            return id;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Item getItemById(SQLiteDatabase db, int id) {
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE _id = "+id+" LIMIT 0,1", null);
            if (cursor.moveToFirst()) {
                return new Item(cursor);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public static ArrayList<Item> getItemsByParrentId(SQLiteDatabase db, int parent_id) {
        Cursor cursor = null;
        ArrayList<Item> items = new ArrayList<>();
        try {
            cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE parent_id = "+parent_id, null);
            while (cursor.moveToNext()) {
                Item item = new Item(cursor);
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    public static ArrayList<Item> getItemsByFilter(SQLiteDatabase db, String query) {
        Cursor cursor = null;
        ArrayList<Item> items = new ArrayList<>();
        try {
            cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE (name_lowcase LIKE '%"+query.toLowerCase()+"%' OR preview_text LIKE '%"+query.toLowerCase()+"%')  AND last_child = 1 LIMIT 0,12 ", null);
            while (cursor.moveToNext()) {
                Item item = new Item(cursor);
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }


    public static ArrayList<Item> getAllItems(SQLiteDatabase db) {
        Cursor cursor = null;
        ArrayList<Item> items = new ArrayList<>();
        try {
            cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE 1", null);
            while (cursor.moveToNext()) {
                Item item = new Item(cursor);
                items.add(item);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            return items;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }


    public static boolean clear(SQLiteDatabase db) {
        try {
            db.execSQL("DELETE FROM "+TABLE_NAME, new String[] {});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getId() {
        return id;
    }

    public String getName(Context context) {
        try {
            String locale = LangUtils.getLocale(context);
            if (locale.equals(LangUtils.RUS)) {
                return name.optString("rus");
            }
            if (locale.equals(LangUtils.ENG)) {
                return name.optString("eng");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "-";
    }

    public String getDescription(Context context) {
        try {
            String locale = LangUtils.getLocale(context);
            if (locale.equals(LangUtils.RUS)) {
                return detail_text.optString("rus");
            }
            if (locale.equals(LangUtils.ENG)) {
                return detail_text.optString("eng");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "-";
    }


    public JSONObject getData() {
        return data;
    }

    public int getParentId() {
        return parent_id;
    }

    public File getPictureFile(Context context) {
        return new File(context.getCacheDir()+"/"+preview_picture.split("/")[preview_picture.split("/").length-1]);
    }

    public String getPictureURL() {
        return preview_picture;
    }


    public JSONObject toJsonObject() {
        try {
            JSONObject object = new JSONObject();
            object.put("id", id);
            object.put("name", name);
            object.put("preview_picture", preview_picture);
            object.put("detail_text", detail_text);

            object.put("parent_id",parent_id);

            object.put("price",data.optInt("price"));
            object.put("prop",data.optJSONArray("prop"));
            object.put("sent",data.optBoolean("sent", false));

            object.put("min_count",data.optInt("min_count", 0));

            object.put("last_child",data.optBoolean("last_child", false));

            JSONArray jsonArray = new JSONArray();
            for (Item item:subItems) {
                jsonArray.put(item.toJsonObject());
            }
            object.put("data", jsonArray);

            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
