package com.duxcast.bohorest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.duxcast.bohorest.R;
import com.duxcast.bohorest.db.DbHelper;
import com.duxcast.bohorest.dialogs.ConnectionDialog;
import com.duxcast.bohorest.dialogs.MessageDialog;
import com.duxcast.bohorest.model.Item;

import java.io.File;

public class ImageViewerActivity extends FragmentActivity {

    static ConnectionDialog connectionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageviewer_activity);

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectionDialog.action);
        registerReceiver(receiver, mIntentFilter);

        if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.app_name));

        int id = getIntent().getIntExtra("item_id", -1);
        Item item = Item.getItemById(DbHelper.getInstance(getApplicationContext()).getDb(), id);

        SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) findViewById(R.id.image);
        imageView.setImage(ImageSource.uri(item.getPictureFile(getApplicationContext()).getAbsolutePath()));

        findViewById(R.id.backButton).setOnClickListener(view -> finish());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (ConnectionDialog.action.equals(intent.getAction())) {
                    if ("show".equals(intent.getStringExtra("action"))) {
                        if (connectionDialog==null) {
                            connectionDialog = new ConnectionDialog(ImageViewerActivity.this);
                            connectionDialog.show();
                        }
                    } else {
                        if (connectionDialog!=null) {
                            connectionDialog.dismiss();
                            connectionDialog = null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}
