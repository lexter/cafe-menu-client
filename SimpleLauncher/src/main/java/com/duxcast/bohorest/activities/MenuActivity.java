package com.duxcast.bohorest.activities;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.adapters.GridAdapter;
import com.duxcast.bohorest.db.DbHelper;
import com.duxcast.bohorest.dialogs.ConnectionDialog;
import com.duxcast.bohorest.model.Item;

import java.util.ArrayList;

public class MenuActivity extends FragmentActivity {

    GridView listViewItems;
    GridAdapter gridAdapter;
    SearchView search;
    static boolean keyboardShown = false;

    static ConnectionDialog connectionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);

        if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.app_name));

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectionDialog.action);
        registerReceiver(receiver, mIntentFilter);

        listViewItems = (GridView) findViewById(R.id.gridView);
        ArrayList<Item> items = Item.getItemsByParrentId(DbHelper.getInstance(getApplicationContext()).getDb(), 0);
        gridAdapter = new GridAdapter(this,items);
        listViewItems.setAdapter(gridAdapter);
        listViewItems.setOnItemClickListener((adapterView, view, i, l) -> {
            int id = gridAdapter.getItem(i).getId();
            if (!gridAdapter.getItem(i).getData().optBoolean("last_child", false)) {
                if (getActionBar()!=null) getActionBar().setTitle(gridAdapter.getItem(i).getName(getApplicationContext()));
                gridAdapter.clear();
                gridAdapter.addAll(Item.getItemsByParrentId(DbHelper.getInstance(getApplicationContext()).getDb(), id));
                gridAdapter.notifyDataSetChanged();
            } else {
                Intent intent = new Intent(MenuActivity.this, ItemViewerActivity.class);
                intent.putExtra("item_id", id);
                startActivity(intent);
            }
        });

        findViewById(R.id.toorderButton).setOnClickListener(v -> finish());

        findViewById(R.id.backButton).setOnClickListener(view -> {

            if (search!=null) {
                if (keyboardShown) {
                    search.clearFocus();
                    keyboardShown=false;
                    return;
                }
                if (!search.isIconified()) {
                    search.setQuery("", false);
                    search.setIconified(true);
                    return;
                }
            }

            if (gridAdapter.getCount()>0) {
                if (gridAdapter.getItem(0) == null) {
                    finish();
                    return;
                }
                if (gridAdapter.getItem(0).getParentId() == 0) {
                    finish();
                    return;
                }

                Item parrent = Item.getItemById(DbHelper.getInstance(getApplicationContext()).getDb(), gridAdapter.getItem(0).getParentId());
                Item parentOfParent = Item.getItemById(DbHelper.getInstance(getApplicationContext()).getDb(), parrent.getParentId());
                if (parentOfParent!=null) {
                    if (getActionBar()!=null) getActionBar().setTitle(parentOfParent.getName(getApplicationContext()));
                } else {
                    if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.app_name));
                }

                int parent_id = parrent.getParentId();
                gridAdapter.clear();
                gridAdapter.addAll(Item.getItemsByParrentId(DbHelper.getInstance(getApplicationContext()).getDb(), parent_id));
                gridAdapter.notifyDataSetChanged();
            } else {
                finish();
                return;
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.options_menu, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                ArrayList<Item> items;
                if (query != null && query.length() < 3)
                    items = Item.getItemsByParrentId(DbHelper.getInstance(getApplicationContext()).getDb(), 0);
                else
                    items = Item.getItemsByFilter(DbHelper.getInstance(getApplicationContext()).getDb(), query);
                if (getActionBar() != null) getActionBar().setTitle(getString(R.string.app_name));
                gridAdapter.clear();
                gridAdapter.addAll(items);
                gridAdapter.notifyDataSetChanged();
                search.clearFocus();
                keyboardShown = false;
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                ArrayList<Item> items;
                if (query != null && query.length() < 3)
                    items = Item.getItemsByParrentId(DbHelper.getInstance(getApplicationContext()).getDb(), 0);
                else
                    items = Item.getItemsByFilter(DbHelper.getInstance(getApplicationContext()).getDb(), query);
                if (getActionBar() != null) getActionBar().setTitle(getString(R.string.app_name));
                keyboardShown = true;
                gridAdapter.clear();
                gridAdapter.addAll(items);
                gridAdapter.notifyDataSetChanged();
                return true;
            }


        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (ConnectionDialog.action.equals(intent.getAction())) {
                    if ("show".equals(intent.getStringExtra("action"))) {
                        if (connectionDialog==null) {
                            connectionDialog = new ConnectionDialog(MenuActivity.this);
                            connectionDialog.show();
                        }
                    } else {
                        if (connectionDialog!=null) {
                            connectionDialog.dismiss();
                            connectionDialog = null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}
