package com.duxcast.bohorest.activities;

import android.annotation.SuppressLint;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.adapters.ListItemAdapter;
import com.duxcast.bohorest.dialogs.AboutDialog;
import com.duxcast.bohorest.dialogs.ConnectionDialog;
import com.duxcast.bohorest.dialogs.MessageDialog;
import com.duxcast.bohorest.dialogs.SendOrderDialog;
import com.duxcast.bohorest.dialogs.UpdateDialog;
import com.duxcast.bohorest.model.Item;

import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

import org.json.JSONObject;

import java.util.ArrayList;

public class HomeScreenActivity extends FragmentActivity {

    Handler handler;
    DynamicListView listViewItems;
    ListItemAdapter listItemAdapter;
    AlphaInAnimationAdapter animationAdapter;
    ImageView imageViewUserOne,imageViewUserTwo,imageViewUserThree,imageViewUserFour,imageViewUserFive,imageViewAddUser;
    Button buttonSendOrder,buttonResetOrder;

//    static ConnectionDialog connectionDialog;

    public static final String UPDATE_ACTION = "com.duxcast.bohorest.activities.HomeScreenActivity.update";

    public static final String SUCCESS_ACTION = "com.duxcast.bohorest.activities.HomeScreenActivity.success";
    public static final String PROCESSED_ACTION = "com.duxcast.bohorest.activities.HomeScreenActivity.processed";
    public static final String ERROR_ACTION = "com.duxcast.bohorest.activities.HomeScreenActivity.errorsend";

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen_activity);

        if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.app_name));

        handler = new Handler(Looper.getMainLooper());

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(UPDATE_ACTION);
        mIntentFilter.addAction(SUCCESS_ACTION);
        mIntentFilter.addAction(PROCESSED_ACTION);
        mIntentFilter.addAction(ERROR_ACTION);
        mIntentFilter.addAction(ConnectionDialog.action);
        mIntentFilter.addAction(UpdateDialog.action);
        registerReceiver(receiver, mIntentFilter);

        listViewItems = (DynamicListView) findViewById(R.id.gridView2);

        listItemAdapter = new ListItemAdapter(this, new ArrayList<>());
        animationAdapter = new AlphaInAnimationAdapter(listItemAdapter);
        animationAdapter.setAbsListView(listViewItems);
        listViewItems.setAdapter(animationAdapter);

        listViewItems.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(HomeScreenActivity.this, ItemViewerActivity.class);
            intent.putExtra("item_id", ((Item) listViewItems.getAdapter().getItem(i)).getId());
            startActivity(intent);
        });


        findViewById(R.id.addItem).setOnClickListener(view -> startActivity(new Intent(HomeScreenActivity.this, MenuActivity.class)));

        findViewById(R.id.buttonOpenMenu).setOnClickListener(view -> startActivity(new Intent(HomeScreenActivity.this, MenuActivity.class)));

        imageViewUserOne = (ImageView) findViewById(R.id.imageViewUserOne);
        imageViewUserOne.setTag(1);
        imageViewUserOne.setOnClickListener(usersClickListener);

        imageViewUserTwo = (ImageView) findViewById(R.id.imageViewUserTwo);
        imageViewUserTwo.setTag(2);
        imageViewUserTwo.setOnClickListener(usersClickListener);

        imageViewUserThree = (ImageView) findViewById(R.id.imageViewUserThree);
        imageViewUserThree.setTag(3);
        imageViewUserThree.setOnClickListener(usersClickListener);

        imageViewUserFour = (ImageView) findViewById(R.id.imageViewUserFour);
        imageViewUserFour.setTag(4);
        imageViewUserFour.setOnClickListener(usersClickListener);

        imageViewUserFive = (ImageView) findViewById(R.id.imageViewUserFive);
        imageViewUserFive.setTag(5);
        imageViewUserFive.setOnClickListener(usersClickListener);

        imageViewAddUser = (ImageView) findViewById(R.id.imageViewAddUser);
        imageViewAddUser.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this,R.style.AppCompatAlertDialogStyle);
            builder.setTitle(getString(R.string.add_guest));
            builder.setMessage(getString(R.string.add_guest_desc));
            builder.setPositiveButton(getString(R.string.add), (dialog, which) -> {
                try {
                    JSONObject object = new JSONObject();
                    object.put("action", "addUser");
                    object.put("table", SharedPrefUtils.getTableNum(getApplicationContext()));
                    Intent intent = new Intent(SocketService.broadcastAction);
                    intent.putExtra("message", object.toString());
                    sendBroadcast(intent);
                    updateViews();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
            builder.setNegativeButton(getString(R.string.cancel), (dialog, which) -> {

            });
            AlertDialog alert = builder.create();
            alert.show();
        });

        buttonSendOrder = (Button) findViewById(R.id.buttonSendOrder);
        buttonSendOrder.setOnClickListener(v -> {
            new SendOrderDialog(HomeScreenActivity.this).show();
        });

        buttonResetOrder = (Button) findViewById(R.id.buttonResetOrder);
        buttonResetOrder.setOnClickListener(v -> {
            try {
                JSONObject object = new JSONObject();
                object.put("action", "clearList");
                object.put("table", SharedPrefUtils.getTableNum(getApplicationContext()));
                object.put("orderNum",SharedPrefUtils.getOrderNum(getApplicationContext()));
                Intent intent = new Intent(SocketService.broadcastAction);
                intent.putExtra("message", object.toString());
                sendBroadcast(intent);
                updateViews();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        startService(new Intent(getApplicationContext(), SocketService.class));
    }

    View.OnClickListener usersClickListener = v -> {
        SharedPrefUtils.setOrderNum(getApplicationContext(), (int) v.getTag());
        updateViews();
    };

    void selectUser(int num) {
        if (num==1) imageViewUserOne.setSelected(true);
        else imageViewUserOne.setSelected(false);

        if (num==2) imageViewUserTwo.setSelected(true);
        else imageViewUserTwo.setSelected(false);

        if (num==3)imageViewUserThree.setSelected(true);
        else imageViewUserThree.setSelected(false);

        if (num==4) imageViewUserFour.setSelected(true);
        else imageViewUserFour.setSelected(false);

        if (num==5) imageViewUserFive.setSelected(true);
        else imageViewUserFive.setSelected(false);
    }

    void changeUserCount(int count) {
        if (count>0) imageViewUserOne.setVisibility(View.VISIBLE);
        else imageViewUserOne.setVisibility(View.GONE);

        if (count>1) imageViewUserTwo.setVisibility(View.VISIBLE);
        else imageViewUserTwo.setVisibility(View.GONE);

        if (count>2) imageViewUserThree.setVisibility(View.VISIBLE);
        else imageViewUserThree.setVisibility(View.GONE);

        if (count>3) imageViewUserFour.setVisibility(View.VISIBLE);
        else imageViewUserFour.setVisibility(View.GONE);

        if (count>4) imageViewUserFive.setVisibility(View.VISIBLE);
        else imageViewUserFive.setVisibility(View.GONE);

        if (count<5) imageViewAddUser.setVisibility(View.VISIBLE);
        else imageViewAddUser.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionDialog.dialog = new ConnectionDialog(HomeScreenActivity.this);
        updateViews();
    }

    public void updateViews() {
        ArrayList<Item> items = SharedPrefUtils.getArrayList(getApplicationContext());
        listItemAdapter.clear();
        listItemAdapter.addAll(items);
        listItemAdapter.notifyDataSetChanged();

        if (SharedPrefUtils.getOrderNum(getApplicationContext())>SharedPrefUtils.countUsers(getApplicationContext()))
            SharedPrefUtils.setOrderNum(getApplicationContext(),SharedPrefUtils.countUsers(getApplicationContext()));


        if (SharedPrefUtils.isSentSelectedOrder(getApplicationContext())) buttonResetOrder.setVisibility(View.GONE);
        else buttonResetOrder.setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.textViewTotal)).setText(String.format(getString(R.string.in_total), SharedPrefUtils.totalSumItem(getApplicationContext()), getString(R.string.valute)));
        int userscount = SharedPrefUtils.countUsers(getApplicationContext());
        changeUserCount(userscount);
        selectUser(SharedPrefUtils.getOrderNum(getApplicationContext()));

        if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.table_number)+" "+SharedPrefUtils.getTableNum(getApplicationContext()));

        if (items.size()==0) {
            findViewById(R.id.footer).setVisibility(View.GONE);
            findViewById(R.id.buttonOpenMenuLayout).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.textViewHeader)).setText(String.format(getString(R.string.order_guest_empty), SharedPrefUtils.getOrderNum(getApplicationContext())));
        } else {
            findViewById(R.id.footer).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonOpenMenuLayout).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.textViewHeader)).setText(String.format(getString(R.string.order_guest),SharedPrefUtils.getOrderNum(getApplicationContext())));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                new AboutDialog(this).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    public static void updateViews(Context context) {
        Intent intentBroadcast = new Intent(UPDATE_ACTION);
        context.sendBroadcast(intentBroadcast);
    }


    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.wtf(this.getClass().getName(), ConnectionDialog.action);
                if (ConnectionDialog.action.equals(intent.getAction())) {
                    if ("show".equals(intent.getStringExtra("action"))) {
                        if (ConnectionDialog.dialog!=null) {
                            if (!ConnectionDialog.dialog.isShowing()) ConnectionDialog.dialog.show();
                        }

                    } else {
                        if (ConnectionDialog.dialog!=null) ConnectionDialog.dialog.dismiss();
                    }
                }

                if (UpdateDialog.action.equals(intent.getAction())) {
                    new UpdateDialog(HomeScreenActivity.this,new JSONObject(intent.getStringExtra("base"))).show();
                }

                if (SUCCESS_ACTION.equals(intent.getAction())) {
                    new MessageDialog(HomeScreenActivity.this,getString(R.string.message_success)).show();
                }

                if (PROCESSED_ACTION.equals(intent.getAction())) {
                    new MessageDialog(HomeScreenActivity.this,getString(R.string.message_processed)).show();
                }

                if (ERROR_ACTION.equals(intent.getAction())) {
                    new MessageDialog(HomeScreenActivity.this,getString(R.string.message_error)).show();
                }

                if (UPDATE_ACTION.equals(intent.getAction())) {
                    updateViews();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
