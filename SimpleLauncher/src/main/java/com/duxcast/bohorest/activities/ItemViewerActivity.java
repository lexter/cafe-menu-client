package com.duxcast.bohorest.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.duxcast.bohorest.R;
import com.duxcast.bohorest.db.DbHelper;
import com.duxcast.bohorest.dialogs.ConnectionDialog;
import com.duxcast.bohorest.model.Item;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONArray;
import org.json.JSONObject;


public class ItemViewerActivity extends FragmentActivity {

    ImageView imageView;
    TextView textViewName,textViewDescription;

    static ConnectionDialog connectionDialog;

    Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.itemviewer_activity);

        if (getActionBar()!=null) getActionBar().setTitle(getString(R.string.app_name));

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ConnectionDialog.action);
        mIntentFilter.addAction(HomeScreenActivity.UPDATE_ACTION);
        registerReceiver(receiver, mIntentFilter);

        int id = getIntent().getIntExtra("item_id", -1);
        item = Item.getItemById(DbHelper.getInstance(getApplicationContext()).getDb(), id);

        imageView = (ImageView) findViewById(R.id.imageViewItem);
        Glide.with(getApplicationContext())
            .load(item.getPictureFile(getApplicationContext()))
            .crossFade()
            .into(imageView);
        imageView.setOnClickListener(view -> {
            Intent intent = new Intent(ItemViewerActivity.this, ImageViewerActivity.class);
            intent.putExtra("item_id", item.getId());
            startActivity(intent);
        });

        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewName.setText(item.getName(getApplicationContext()));

        findViewById(R.id.toorderButton).setOnClickListener(v -> finish());

        String param = "";
        JSONArray paramJson = item.getData().optJSONArray("prop");
        if (paramJson!=null)
        for (int i=0;i<paramJson.length();i++) {
            param += paramJson.optJSONObject(i).optString("name")+" : "+paramJson.optJSONObject(i).optString("value")+"\n";
        }

        if (!TextUtils.isEmpty(param)) {
            findViewById(R.id.layoutKkalDesc).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.textViewParam)).setText(param);
            findViewById(R.id.textViewParam).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.layoutKkalDesc).setVisibility(View.GONE);
            findViewById(R.id.textViewParam).setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(item.getDescription(getApplicationContext()))) {
            findViewById(R.id.layoutDesc).setVisibility(View.VISIBLE);
            textViewDescription = (TextView) findViewById(R.id.textViewDesc);
            textViewDescription.setText(item.getDescription(getApplicationContext()));
        } else {
            findViewById(R.id.layoutDesc).setVisibility(View.GONE);
        }

        int price = item.getData().optInt("price", -1);
        TextView textViewPrice = (TextView) findViewById(R.id.textViewPrice);
        if (price!=-1) {
            textViewPrice.setVisibility(View.VISIBLE);
            textViewPrice.setText(String.format("%s %s",price,getString(R.string.valute)));
        } else {
            textViewPrice.setVisibility(View.INVISIBLE);
        }

        findViewById(R.id.buttonAddToList).setOnClickListener(view -> {
            addToList(item, getApplicationContext());
            Toast.makeText(getApplicationContext(), R.string.item_added, Toast.LENGTH_SHORT).show();
        });

        findViewById(R.id.buttonUp).setOnClickListener(view -> {
            addToList(item, getApplicationContext());
        });

        findViewById(R.id.buttonDown).setOnClickListener(view -> {
            if (SharedPrefUtils.minCountItem(item.getId(),getApplicationContext())==SharedPrefUtils.countItem(item.getId(),getApplicationContext())) {
                Toast.makeText(getApplicationContext(),R.string.order_already_send,Toast.LENGTH_SHORT).show();
                return;
            }
            removeFromList(item, getApplicationContext());
        });

        if (SharedPrefUtils.isSentItem(item,getApplicationContext())) findViewById(R.id.buttonRemoveItem).setVisibility(View.GONE);
        findViewById(R.id.buttonRemoveItem).setOnClickListener(view -> {
            removeAllFromList(item, getApplicationContext());
        });

        findViewById(R.id.backButton).setOnClickListener(view -> finish());

        updateCountViews(getApplicationContext(),item);

    }


    void updateCountViews(Context context,Item currentItem) {
        JSONArray items = SharedPrefUtils.getList(context);
        int countPrice,count;
        for (int i=0;i<items.length();i++) {
            if (currentItem.getId()==items.optJSONObject(i).optInt("id")) {
                count=items.optJSONObject(i).optInt("count");

                findViewById(R.id.layoutAddToList).setVisibility(View.GONE);
                findViewById(R.id.layoutChangeCount).setVisibility(View.VISIBLE);

                if (count>0) countPrice=items.optJSONObject(i).optInt("count")*items.optJSONObject(i).optInt("price");
                else countPrice=items.optJSONObject(i).optInt("price");
                ((TextView) findViewById(R.id.textViewPrice)).setText(String.format("%s %s",countPrice,getString(R.string.valute)));
                ((Button) findViewById(R.id.buttonCount)).setText(String.format("%s %s",count,getString(R.string.count)));
                return;
            }
        }
        findViewById(R.id.layoutAddToList).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutChangeCount).setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    synchronized void addToList(Item item,Context context) {
        try {
            JSONObject object = new JSONObject();
            object.put("orderNum",SharedPrefUtils.getOrderNum(context));
            object.put("itemId", item.getId());
            object.put("table", SharedPrefUtils.getTableNum(context));
            object.put("action", "addItem");
            Intent intent = new Intent(SocketService.broadcastAction);
            intent.putExtra("message", object.toString());
            context.sendBroadcast(intent);
            HomeScreenActivity.updateViews(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized void removeFromList(Item item,Context context) {
        try {
            JSONObject object = new JSONObject();
            object.put("orderNum",SharedPrefUtils.getOrderNum(context));
            object.put("itemId", item.getId());
            object.put("table", SharedPrefUtils.getTableNum(context));
            object.put("action", "removeItem");
            Intent intent = new Intent(SocketService.broadcastAction);
            intent.putExtra("message", object.toString());
            context.sendBroadcast(intent);
            HomeScreenActivity.updateViews(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized void removeAllFromList(Item item,Context context) {
        try {
            JSONObject object = new JSONObject();
            object.put("orderNum",SharedPrefUtils.getOrderNum(context));
            object.put("itemId", item.getId());
            object.put("table", SharedPrefUtils.getTableNum(context));
            object.put("action", "removeAllItems");
            Intent intent = new Intent(SocketService.broadcastAction);
            intent.putExtra("message", object.toString());
            context.sendBroadcast(intent);
            HomeScreenActivity.updateViews(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (HomeScreenActivity.UPDATE_ACTION.equals(intent.getAction())) {
                    if (item!=null) updateCountViews(getApplicationContext(),item);
                }
                if (ConnectionDialog.action.equals(intent.getAction())) {
                    if ("show".equals(intent.getStringExtra("action"))) {
                        if (connectionDialog==null) {
                            connectionDialog = new ConnectionDialog(ItemViewerActivity.this);
                            connectionDialog.show();
                        }
                    } else {
                        if (connectionDialog!=null) {
                            connectionDialog.dismiss();
                            connectionDialog = null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}
