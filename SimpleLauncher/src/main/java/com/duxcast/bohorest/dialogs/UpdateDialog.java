package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.db.DbHelper;
import com.duxcast.bohorest.model.Item;
import com.duxcast.bohorest.utils.BitmapUtils;
import com.duxcast.bohorest.utils.HttpCommand;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class UpdateDialog extends AlertDialog {

    Handler handler;

    public static String action = "com.duxcast.dialog.UpdateDialog";

    int i=0;

    public UpdateDialog(final Context context, final JSONObject js) {
        super(context, R.style.AppCompatAlertDialogStyle);

        handler = new Handler(Looper.getMainLooper());

        new Thread(() -> {
            try {
                SharedPrefUtils.setLastUpdate(context, js.optLong("date"));
                JSONArray jsonArray = js.optJSONArray("data");
                Item.clear(DbHelper.getInstance(context).getDb());
                for (i=0;i<jsonArray.length();i++) {
                    new Item(jsonArray.optJSONObject(i),context).save(DbHelper.getInstance(context).getDb());
                    handler.post(()-> setMessage("Обновлеие категории "+ i +"/"+jsonArray.length()));
                }

                final ArrayList<Item> items = Item.getAllItems(DbHelper.getInstance(context).getDb());
                for (i=0; i<items.size();i++) {
                    BitmapUtils.getImage(context, items.get(i));
                    handler.post(() -> setMessage("Загрузка изображения " + i + "/" + items.size()));
                }
                Thread.sleep(2000);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            handler.post(this::dismiss);
        }).start();

        setTitle("Обновление базы");
        setMessage("Запрос");
        setCancelable(false);
    }


}