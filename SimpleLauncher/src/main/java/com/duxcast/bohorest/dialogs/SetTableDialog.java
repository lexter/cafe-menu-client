package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.activities.HomeScreenActivity;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;


public class SetTableDialog extends AlertDialog {

    Handler handler;

    public static String action = "com.duxcast.dialog.SendOrderDialog";

    Button buttonOk,buttonCancel;
    NumberPicker np;


    public SetTableDialog(final Context context) {

        super(context, R.style.AppCompatAlertDialogStyle);

        View view =  LayoutInflater.from(context).inflate(R.layout.settable_dialog, null);

        handler = new Handler(Looper.getMainLooper());

        np = (NumberPicker) view.findViewById(R.id.numberPicker);
        np.setMinValue(0);
        np.setMaxValue(100);
        np.setValue(SharedPrefUtils.getTableNum(context));
        np.setWrapSelectorWheel(false);

        buttonOk = (Button) view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(v -> {
            try {
                SharedPrefUtils.setTableNum(context, np.getValue());
                Intent intentForSend = new Intent(SocketService.broadcastAction);
                JSONObject object = new JSONObject();
                object.put("action", "setTable");
                object.put("table", SharedPrefUtils.getTableNum(context));
                intentForSend.putExtra("message", object.toString());
                context.sendBroadcast(intentForSend);
                dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
        buttonCancel = (Button) view.findViewById(R.id.buttonClose);
        buttonCancel.setOnClickListener(v -> dismiss());



        setTitle(R.string.set_table);
        setView(view);
        setCancelable(true);
    }


}