package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;

public class ServerParamsDialog extends AlertDialog {

    public static String action = "com.duxcast.dialog.ServerParamsDialog";

    private EditText editTextIpAddress,editTextPort;

    public ServerParamsDialog(final Context context) {
        super(context, R.style.AppCompatAlertDialogStyle);

        View view =  LayoutInflater.from(context).inflate(R.layout.serverparams_dialog, null);

        editTextIpAddress = (EditText) view.findViewById(R.id.editTextIpAddress);
        editTextIpAddress.setText(SharedPrefUtils.getServerIp(context));

        editTextPort = (EditText) view.findViewById(R.id.editTextPort);
        editTextPort.setText(String.valueOf(SharedPrefUtils.getServerPort(context)));

        ((Button) view.findViewById(R.id.buttonSubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SharedPrefUtils.setServerIp(context, editTextIpAddress.getText().toString());
                    SharedPrefUtils.setServerPort(context, Integer.valueOf(editTextPort.getText().toString()));
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, "ERROR NUMBER", Toast.LENGTH_SHORT).show();
                }
            }
        });

        setTitle(context.getString(R.string.server_connection));
        setView(view);
        setCancelable(false);
    }



}