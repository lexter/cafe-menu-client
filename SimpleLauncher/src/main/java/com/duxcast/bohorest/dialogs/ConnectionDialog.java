package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.duxcast.bohorest.R;

public class ConnectionDialog extends AlertDialog {

    public static String action = "com.duxcast.dialog.ConnectionDialog";
    public static volatile ConnectionDialog dialog;

    public ConnectionDialog(final Context context) {
        super(context, R.style.AppCompatAlertDialogStyle);

        View view = LayoutInflater.from(context).inflate(R.layout.connection_dialog, null);

        view.findViewById(R.id.buttonSettingDialog).setOnClickListener(v -> new AccessKeyDialog(context).show());

        setTitle(R.string.server_connection);
        setView(view);

        setCancelable(false);
    }

    public static void show(Context context) {
        Intent intentBroadcast = new Intent(action);
        intentBroadcast.putExtra("action", "show");
        context.sendBroadcast(intentBroadcast);
    }


    public static void hide(Context context) {
        Log.wtf(ConnectionDialog.class.getName(), action);
        Intent intentBroadcast = new Intent(action);
        intentBroadcast.putExtra("action", "hide");
        context.sendBroadcast(intentBroadcast);
    }

}