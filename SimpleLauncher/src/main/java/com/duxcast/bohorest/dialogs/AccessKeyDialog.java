package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.utils.SharedPrefUtils;

public class AccessKeyDialog extends AlertDialog {

    public static String action = "com.duxcast.dialog.AccessKeyDialog";

    public AccessKeyDialog(final Context context) {
        super(context, R.style.AppCompatAlertDialogStyle);

        View view =  LayoutInflater.from(context).inflate(R.layout.accesskey_dialog, null);

        final EditText editTextNumber = (EditText) view.findViewById(R.id.editTextKey);

        view.findViewById(R.id.buttonSubmit).setOnClickListener(v -> {
            try {
                if (editTextNumber.getText().toString().equals("8516")) {
                    new ServiceDialog(context).show();
                    dismiss();
                }
                if (editTextNumber.getText().toString().equals("4639")) {
                    new SetTableDialog(context).show();
                    dismiss();
                } else {
                    Toast.makeText(context, "ERROR NUMBER", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "ERROR NUMBER", Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.buttonClose).setOnClickListener(v -> dismiss());

        setTitle(context.getString(R.string.access_key));

        setView(view);

        setCancelable(true);
    }


}