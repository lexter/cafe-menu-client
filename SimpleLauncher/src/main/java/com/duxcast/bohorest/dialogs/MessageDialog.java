package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.activities.HomeScreenActivity;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;


public class MessageDialog extends AlertDialog {

    Handler handler;

    public static String action = "com.duxcast.dialog.MessageDialog";

    public MessageDialog(final Context context, String message) {

        super(context, R.style.AppCompatAlertDialogStyle);

        handler = new Handler(Looper.getMainLooper());

        setTitle(R.string.message);
        setMessage(message);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(8000);
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                    dismiss();
                }
            }
        }).start();

        setCancelable(true);
    }


}