package com.duxcast.bohorest.dialogs;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.activities.HomeScreenActivity;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;

import java.io.File;

public class ServiceDialog extends AlertDialog {

    public static String action = "com.duxcast.dialog.ServiceDialog";


    public ServiceDialog(final Context context) {
        super(context, R.style.AppCompatAlertDialogStyle);

        View view =  LayoutInflater.from(context).inflate(R.layout.service_dialog, null);

        view.findViewById(R.id.buttonCloseDialog).setOnClickListener(v -> dismiss());

        view.findViewById(R.id.buttonOpenSystemActivity).setOnClickListener(v -> {
            Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });

        view.findViewById(R.id.buttonUpdateBase).setOnClickListener(v -> {
            try {
                SharedPrefUtils.setLastUpdate(getContext(), SharedPrefUtils.getLastUpdate(getContext())-1);
                JSONObject object = new JSONObject();
                object.put("action", "checkLastBaseUpdate");
                Intent intent = new Intent(SocketService.broadcastAction);
                intent.putExtra("message", object.toString());
                context.sendBroadcast(intent);
            } catch (Exception e) {
                Toast.makeText(getContext(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.buttonSetTable).setOnClickListener(v -> new SetTableDialog(context).show());

        view.findViewById(R.id.buttonServerSettings).setOnClickListener(v -> new ServerParamsDialog(context).show());

        setTitle(context.getString(R.string.service_menu));
        setView(view);

        setCancelable(true);
    }


}