package com.duxcast.bohorest.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.support.v7.app.AlertDialog;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.utils.LangUtils;
import com.duxcast.bohorest.utils.SharedPrefUtils;

public class AboutDialog extends AlertDialog {

    public static String action = "com.duxcast.dialog.AboutDialog";

    public AboutDialog(final Context context) {
        super(context, R.style.AppCompatAlertDialogStyle);

        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        String [] splitIp = ip.split("\\.");
        if (splitIp.length>2) ip = splitIp[splitIp.length-2]+"."+splitIp[splitIp.length-1];

        View view =  LayoutInflater.from(context).inflate(R.layout.about_dialog, null);
        ((TextView)view.findViewById(R.id.textViewNumTablet)).setText(context.getString(R.string.tablet_number) + ip);
        ((TextView)view.findViewById(R.id.textViewNumTable)).setText(context.getString(R.string.table_number) + SharedPrefUtils.getTableNum(context));
        view.findViewById(R.id.buttonCloseDialog).setOnClickListener(v -> dismiss());

        view.findViewById(R.id.buttonOpenServiceMenu).setOnClickListener(v -> {
            new AccessKeyDialog(context).show();
            dismiss();
        });

        view.findViewById(R.id.buttonChLang).setOnClickListener(v -> {
            String locale = LangUtils.getLocale(context);
            if (locale.equals(LangUtils.RUS)) LangUtils.setLocale((Activity) context, "en");
            else LangUtils.setLocale((Activity) context, "ru");
        });


        try {
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            ((TextView) view.findViewById(R.id.textViewVersion)).setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        int batteryPct = (int)(((float)level / (float)scale) * (float)100);
        TextView batStat = (TextView) view.findViewById(R.id.textViewBattery);
        batStat.setText(String.format(context.getString(R.string.status_battery), batteryPct));
        if (batteryPct<10) {
            batStat.setTextColor(Color.RED);
        } else {
            batStat.setTextColor(Color.BLACK);
        }

        setTitle(context.getString(R.string.about));
        setView(view);

        setCancelable(true);
    }

}