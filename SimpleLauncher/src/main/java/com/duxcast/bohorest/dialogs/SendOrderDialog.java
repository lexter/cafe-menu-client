package com.duxcast.bohorest.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.activities.HomeScreenActivity;
import com.duxcast.bohorest.services.SocketService;
import com.duxcast.bohorest.utils.SharedPrefUtils;

import org.json.JSONObject;


public class SendOrderDialog extends AlertDialog {

    Handler handler;

    public static String action = "com.duxcast.dialog.SendOrderDialog";

    Button buttonOnlyOne,buttonAll,buttonSend,buttonCancel;

    public SendOrderDialog(final Context context) {

        super(context, R.style.AppCompatAlertDialogStyle);

        View view =  LayoutInflater.from(context).inflate(R.layout.sendorder_dialog, null);

        handler = new Handler(Looper.getMainLooper());

        buttonOnlyOne = (Button) view.findViewById(R.id.buttonSendOnlyThis);
        buttonOnlyOne.setOnClickListener(v -> {
            try {
                JSONObject object = new JSONObject();
                object.put("orderNum", SharedPrefUtils.getOrderNum(getContext()));
                object.put("table", SharedPrefUtils.getTableNum(getContext()));
                object.put("action", "acceptOrder");
                Intent intent = new Intent(SocketService.broadcastAction);
                intent.putExtra("message", object.toString());
                context.sendBroadcast(intent);
                HomeScreenActivity.updateViews(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dismiss();
        });
        buttonAll = (Button) view.findViewById(R.id.buttonSendAll);
        buttonAll.setOnClickListener(v -> {
            try {
                JSONObject object = new JSONObject();
                object.put("table", SharedPrefUtils.getTableNum(getContext()));
                object.put("action", "acceptAllOrders");
                Intent intent = new Intent(SocketService.broadcastAction);
                intent.putExtra("message", object.toString());
                context.sendBroadcast(intent);
                HomeScreenActivity.updateViews(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dismiss();
        });
        buttonSend = (Button) view.findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(v -> {
            try {
                JSONObject object = new JSONObject();
                object.put("orderNum", SharedPrefUtils.getOrderNum(getContext()));
                object.put("table", SharedPrefUtils.getTableNum(getContext()));
                object.put("action", "acceptOrder");
                Intent intent = new Intent(SocketService.broadcastAction);
                intent.putExtra("message", object.toString());
                context.sendBroadcast(intent);
                HomeScreenActivity.updateViews(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dismiss();
        });
        buttonCancel = (Button) view.findViewById(R.id.buttonClose);
        buttonCancel.setOnClickListener(v -> dismiss());

        if (SharedPrefUtils.countUsers(context) > 1) {
            buttonOnlyOne.setVisibility(View.VISIBLE);
            buttonAll.setVisibility(View.VISIBLE);
            buttonSend.setVisibility(View.GONE);
        } else {
            buttonOnlyOne.setVisibility(View.GONE);
            buttonAll.setVisibility(View.GONE);
            buttonSend.setVisibility(View.VISIBLE);
        }

        setTitle(R.string.send_order);
        setView(view);
        setCancelable(true);
    }

}