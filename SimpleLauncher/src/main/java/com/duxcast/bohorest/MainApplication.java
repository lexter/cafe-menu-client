package com.duxcast.bohorest;

import android.app.Application;

import com.duxcast.bohorest.utils.TypefaceUtil;

/**
 * Created by admin on 18.03.16.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "font.ttf");
    }

}
