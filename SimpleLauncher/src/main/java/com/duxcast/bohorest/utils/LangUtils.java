package com.duxcast.bohorest.utils;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class LangUtils {

    public static final String RUS = "ru";
    public static final String ENG = "en";

    public static void setLocale(Activity context,String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        context.finish();
    }

    public static String getLocale(Context context) {
        Resources res = context.getResources();
        Configuration conf = res.getConfiguration();
        return conf.locale.getLanguage();
    }



}
