package com.duxcast.bohorest.utils;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpCommand {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static String executeForString(String url) throws Exception {

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
//                .add("email", "Jurassic@Park.com")
//                .add("tel", "90301171XX")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }



}