package com.duxcast.bohorest.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.duxcast.bohorest.R;
import com.duxcast.bohorest.model.Item;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SharedPrefUtils {

    static SharedPreferences sPref;

    public static String getServerIp(Context context) {
        return getParam("serverip", context, "");
    }

    public static void setServerIp(Context context,String ip) {
        setParam("serverip",ip,context);
    }

    public static int getOrderNum(Context context) {
        return Integer.parseInt(getParam("ordernum", context, "1"));
    }

    public static void setOrderNum(Context context,int num) {
        setParam("ordernum",String.valueOf(num),context);
    }

    public static int getTableNum(Context context) {
        return Integer.parseInt(getParam("table", context, "0"));
    }

    public static void setTableNum(Context context,int num) {
        setParam("table",String.valueOf(num),context);
    }


    public static int getServerPort(Context context) {
        return Integer.valueOf(getParam("port", context, "9999"));
    }

    public static void setServerPort(Context context,int number) {
        setParam("port",String.valueOf(number),context);
    }

    public static long getLastUpdate(Context context) {
        return Long.valueOf(getParam("lastupdate", context, "0"));
    }

    public static void setLastUpdate(Context context,long number) {
        setParam("lastupdate",String.valueOf(number),context);
    }


    public static void replaceData(JSONArray jsonArray, Context context) {
        try {
            System.out.println("replace "+jsonArray.toString());
            setParam("orders", jsonArray.toString(), context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int countUsers(Context context) {
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            return jsonArray.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int countItem(int id, Context context) {
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        if (items.optJSONObject(j).optInt("id") == id) {
                            return items.optJSONObject(j).optInt("count", 0);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int minCountItem(int id, Context context) {
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        if (items.optJSONObject(j).optInt("id") == id) {
                            return items.optJSONObject(j).optInt("min_count", 0);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int totalSumItem(Context context) {
        int sum = 0;
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    JSONArray items = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items.length(); j++) {
                        sum += items.optJSONObject(j).optInt("count", 0) * items.optJSONObject(j).optInt("price", 0);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sum;
    }

    public static JSONArray getList(Context context) {
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    return jsonArray.optJSONObject(i).optJSONArray("items");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }

    public static ArrayList<Item> getArrayList(Context context) {
        ArrayList<Item> items = new ArrayList<Item>();
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    JSONArray items1 = jsonArray.optJSONObject(i).optJSONArray("items");
                    for (int j = 0; j < items1.length(); j++) {
                        items.add(new Item(items1.optJSONObject(j), context));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }


    public static boolean isSentItem(Item item,Context context) {
        ArrayList<Item> items = new ArrayList<Item>();
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    for (int j=0;j<jsonArray.optJSONObject(i).optJSONArray("items").length();j++) {
                        if (jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).optInt("id") == item.getId()) {
                            return jsonArray.optJSONObject(i).optJSONArray("items").getJSONObject(j).optBoolean("sent", false);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isSentSelectedOrder(Context context) {
        ArrayList<Item> items = new ArrayList<Item>();
        try {
            JSONArray jsonArray = new JSONArray(getParam("orders",context,"[]"));
            for (int i=0;i<jsonArray.length();i++) {
                if ((i+1)==getOrderNum(context)) {
                    return jsonArray.optJSONObject(i).optBoolean("sent",false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static JSONArray getTextForSms(Context context) {
        try {
            JSONArray array = new JSONArray(getParam("orders",context,"[]"));
            return array;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void setParam(String param, String value, Context context) {
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor ed = sPref.edit();
        ed.putString(param, value);
        ed.commit();
    }

    private static String getParam(String param, Context context, String defaultParam) {
        sPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sPref.getString(param, defaultParam);
    }

}
