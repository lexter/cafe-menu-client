package com.duxcast.bohorest.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.duxcast.bohorest.model.Item;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by admin on 17.03.16.
 */
public class BitmapUtils {


    public static File getImage(Context context,Item item) {
        try {
            File file = item.getPictureFile(context);
            if (file.exists()) return file;
            if (downloadImage(context, item)) return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getImageLarge(Context context,Item item) {
        try {
            File file = item.getPictureFile(context);
            if (file.exists()) return file;
            if (downloadImage(context, item)) return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static boolean downloadImage(Context context, Item item){
        try {
            File file = item.getPictureFile(context);
            URL url = new URL(item.getPictureURL());
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(file);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
            System.out.println("download "+file.exists()+" "+file.getName());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static Bitmap getBitmapFromPath(File file,int size){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=size;

            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                scale*=2;

            System.out.println("scale : "+scale);

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
